﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fenykepalbum_5_2.AspNetCore.NewDb.Models;

namespace fenykepalbum_5_2.Views
{
    public class Galeria : Controller
    {
        private readonly fenykepContext _context;

        public Galeria(fenykepContext context)
        {
            _context = context;
        }

        // GET: Galeria
        public async Task<IActionResult> Index()
        {
            return View(await _context.Kepek.ToListAsync());
        }

       
    }
}
