﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fenykepalbum_5_2.AspNetCore.NewDb.Models;
using System.IO;
using fenykepalbum_5_2.Models;
using System.Linq.Dynamic.Core;
namespace fenykepalbum_5_2.Views
{
    public class KepeimController : Controller
    {
        private readonly fenykepContext _context;     
        public KepeimController(fenykepContext context)
        {
            _context = context;
        }

        // GET: Kepeks
        public async Task<IActionResult> Index(string szűrő, string rendezés, bool megkellfordítani)
        {   
           
            if (szűrő is null)
            {       
                    szűrő = "";
            }
            ViewData["szűrő"] = szűrő;
           
            if (ViewData["rendezés"] !=null)
            {
                rendezés =(string) ViewData["rendezés"]; //ha korábban volt beállítva valamilyen rendezettség
            }

            if ( String.IsNullOrEmpty(rendezés) ) 
            {
                rendezés = "Mikor";
            }
            if (megkellfordítani)
            {
                rendezés = rendezés.Contains("ford") ? rendezés.Substring(0, rendezés.Length - 4) : rendezés + "ford";
            }
            ViewData["rendezés"] = rendezés;

            ViewData["rendfnev"] = "Filenev";
            ViewData["rendhol"] = "Holkeszult";
            ViewData["rendmikor"] = "Mikor";

            if (rendezés == "Filenevford") { ViewData["rendfnev"] = "Filenevford"; }
            if (rendezés == "Holkeszultford") { ViewData["rendhol"] = "Holkeszultford"; }
            if (rendezés == "Mikorford") { ViewData["rendmikor"] = "Mikorford"; }

            if (rendezés.Contains("ford")) { rendezés = rendezés.Substring(0, rendezés.Length - 4) + " DESC"; }
           
            var qo = _context.Kepek.OrderBy(x => x.Id).AsQueryable();
            return View(await qo.OrderBy(rendezés).Where(x => x.Holkeszult.Contains(szűrő) || x.Leiras.Contains(szűrő)).ToListAsync());
                       
        }

        // GET: Kepeks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kepek = await _context.Kepek
                .FirstOrDefaultAsync(m => m.Id == id);
            if (kepek == null)
            {
                return NotFound();
            }

            return View(kepek);
        }

        // GET: Kepeks/Create
        public IActionResult Create()
        {
            Fileok fileok = new Fileok("wwwroot/kepek/");
            var fhasználtak = _context.Kepek.Select(y => y.Filenev).ToList();
            var z=fileok.fnevek.Where(x => !fhasználtak.Contains(x)).Select(q=>q);        
            ViewData["fnevek"] = new SelectList(z.Select(X => new { fn = X }).ToList(), "fn", "fn");         
            return View();
        }

        // POST: Kepeks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Filenev,Holkeszult,Mikor,Leiras")] Kepek kepek)
        {
            if (ModelState.IsValid)
            {
                _context.Add(kepek);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(kepek);
        }

        // GET: Kepeks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kepek = await _context.Kepek.FindAsync(id);
            if (kepek == null)
            {
                return NotFound();
            }
            Fileok fileok = new Fileok("wwwroot/kepek/");         
            ViewData["fnevek"] = new SelectList(fileok.fnevek.Select(X => new { fn = X }).ToList(), "fn", "fn");
            return View(kepek);
        }

        // POST: Kepeks/Edit/5 
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Filenev,Holkeszult,Mikor,Leiras")] Kepek kepek)
        {
            if (id != kepek.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(kepek);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KepekExists(kepek.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(kepek);
        }

        // GET: Kepeks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kepek = await _context.Kepek
                .FirstOrDefaultAsync(m => m.Id == id);
            if (kepek == null)
            {
                return NotFound();
            }

            return View(kepek);
        }

        // POST: Kepeks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var kepek = await _context.Kepek.FindAsync(id);
            _context.Kepek.Remove(kepek);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KepekExists(int id)
        {
            return _context.Kepek.Any(e => e.Id == id);
        }
    }
}
